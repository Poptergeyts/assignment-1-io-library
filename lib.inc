%define COMMAND_READ_CODE 0
%define COMMAND_WRITE_CODE 1
%define COMMAND_EXIT_CODE 60

%define STDIN 0
%define STDOUT 1
%define STDERR 2

%define BASIS 10
%define NEWLINE_CHARACTER 10
%define NULL_CHARACTER 0
%define SPACE_CHARACTER 32
%define TAB_CHARACTER 9

section .text 
; Принимает код возврата и завершает текущий процесс +
exit: 
    mov rax, COMMAND_EXIT_CODE
    syscall
	

; Принимает указатель на нуль-терминированную строку, возвращает её длину +
string_length:
    xor rax, rax
	
	.loop:
		cmp byte[rdi + rax], 0
		jz .end
		inc rax
		jmp .loop
		
	.end:
		ret
		

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
	pop rsi
	
	mov rdx, rax
	mov rax, COMMAND_WRITE_CODE
	mov rdi, STDOUT
	syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	
    mov rax, COMMAND_WRITE_CODE
	mov rsi, rsp
	mov rdi, STDOUT
	mov rdx, 1
	syscall
	
	pop rax
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_CHARACTER
	jmp print_newline


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov rax, rdi
	mov rcx, rsp
	mov rdi, BASIS
	push NULL_CHARACTER
	
	.loop:	;Писать понятные комментарии хороший тон, но я нехороший 
		xor rdx, rdx
		div rdi	
		add rdx, '0'
		dec rsp
		mov byte[rsp], dl
		test rax, rax
		jnz .loop
	
	mov rdi, rsp
	push rcx
	call print_string
	pop rcx
	mov rsp, rcx 
    ret
	

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	test rdi, rdi
	jns print_uint
	
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	
	neg rdi
	jmp print_uint


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
	
    .loop:
		mov dl, [rdi + rcx]
		cmp dl, [rsi + rcx]
		jne .not_equal
		inc rcx
		test dl, dl
		jnz .loop
		mov rax, 1
		jmp .end
		
	.not_equal:
		xor rax, rax
		
	.end:
		ret
		

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, COMMAND_READ_CODE
	mov rdi, STDIN
	mov rdx, 1
	dec rsp
	mov rsi, rsp
	syscall
	test rax, rax
	jz .end
	mov rax, [rsp]
	
	.end:
		inc rsp
		ret 
		

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	push r12
	push r13
	push r14
	
	xor r12, r12
	mov r13, rdi
	mov r14, rsi
	
    .check_start:
		call read_char
		cmp al, SPACE_CHARACTER
		jz .check_start
		cmp al, TAB_CHARACTER
		jz .check_start
		cmp al, NEWLINE_CHARACTER
		jz .check_start
	
	.loop:		
		cmp r14, r12
		jz .overflow
		cmp al, NULL_CHARACTER
		jz .write_last
		cmp al, SPACE_CHARACTER
		jz .write_last
		cmp al, TAB_CHARACTER
		jz .write_last
		cmp al, NEWLINE_CHARACTER
		jz .write_last

		mov [r13 + r12], al
		inc r12
		call read_char
		jmp .loop
		
	.overflow:
		xor rax, rax
		jmp .end
		
	.write_last:
		mov [r13 + r12], al
		mov rax, r13
		mov rdx, r12
		
	.end:
		pop r12
		pop r13
		pop r14
		ret
		
 
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
   xor rax, rax
   xor rcx, rcx
   xor rdx, rdx
   
   .loop:
        mov dl, byte[rdi + rcx]
	sub dl, '0'
        js .out
	cmp dl, '9' - '0' + 1
	jns .out
	inc rcx
	imul rax, BASIS
	add rax, rdx
	jmp .loop
	
    .out:
	mov rdx, rcx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    jz .is_negative
	cmp byte[rdi], '+'
	jnz parse_uint
	inc rdi
    call parse_uint
	inc rdx
	ret 
	
    .is_negative:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
	
	.loop:
		cmp rcx, rdx
		jz .oferflow
		mov rax, [rdi + rcx]
		mov [rsi + rcx], rax
		inc rcx
		test rax, rax
		jz .end
		jmp .loop
		
	.oferflow:
		xor rax, rax
		
    .end:
		ret
